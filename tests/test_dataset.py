import unittest
from unittest import mock
from lxml import etree

from wikisummarizer.etl.dataset import DatasetBuilder


class TestDatasetBuilder(unittest.TestCase):
    def test_getSummary(self):
        domain = "en.wikipedia.org"
        title = "Pi"
        builder = DatasetBuilder(domain)
        summary = builder.getSummary(title)
        self.assertIsNotNone(summary)
        self.assertIsInstance(summary, str)

    def test_getGoodArticles(self):
        domain = "en.wikipedia.org"
        builder = DatasetBuilder(domain)
        limit = 101
        goodArticles = builder.getGoodArticles(limit)
        self.assertIsInstance(goodArticles, set)
        self.assertTrue(len(goodArticles) >= limit)

    def test_getHTML(self):
        domain = "en.wikipedia.org"
        title = "Pi"
        builder = DatasetBuilder(domain)
        html = builder.getHTML(title)
        root = etree.fromstring(html)
        self.assertIsInstance(html, str)
        self.assertEqual(root.xpath("/html/head/title")[0].text, "Pi")

    @unittest.mock.patch.object(DatasetBuilder, "getTitles")
    def test_buildDataset(self, mock):
        domain = "en.wikipedia.org"
        mock.return_value = set(["Pi", "Earth"])
        builder = DatasetBuilder(domain)
        dataset = builder.build()
        self.assertEqual(dataset.shape, (2, 6))
        self.assertEqual(set(dataset.title.values), set(["Earth", "Pi"]))
