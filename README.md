# Wiki summarizer

An experiment on how LLMs work for summarizing wikipedia content

## Local setup
### Requirements

* Python >= 3.10
* Poetry

To prepare local setup run
```> poetry install```

To get a venv shell
```> poetry shell```

To run the tests
```> poetry run python -m unittest discover```