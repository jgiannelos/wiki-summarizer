import logging
import math
import os
import uuid
from datetime import date
from numbers import Number
from urllib.parse import quote

import httpx
import pandas as pd
import tenacity
from trafilatura import extract as extractTextFromHtml


logging.basicConfig(
    level=os.environ.get("LOGLEVEL", "INFO").upper(),
    format="%(asctime)s %(levelname)s %(message)s",
)
logger = logging.getLogger(__name__)


class DatasetBuilder:
    retries = 10

    def __init__(self, domain):
        self.domain = domain

    @tenacity.retry(stop=tenacity.stop_after_attempt(retries))
    def getSummary(self, title: str) -> str | None:
        """Get summary for a given title"""
        logger.info(f"Getting summary for title: {title}")
        baseSummaryURL = f"https://{self.domain}/api/rest_v1/page/summary"
        urlEncodedTitle = quote(title)
        url = f"{baseSummaryURL}/{urlEncodedTitle}"
        res = httpx.get(url, follow_redirects=True)
        res.raise_for_status()
        jsonResponse = res.json()

        if "extract" in jsonResponse.keys():
            logger.debug(f"Succesfully extracted summary for title: {title}")
            return jsonResponse["extract"]

        logger.warn(f"Summary is empty for title: {title}")
        return None

    @tenacity.retry(stop=tenacity.stop_after_attempt(retries))
    def getGoodArticles(
        self, atLeastArticles: Number = math.inf, queryLimit: Number = 100
    ) -> set[str]:
        """Get list of articles assessed as GA"""
        params = {
            "action": "query",
            "list": "categorymembers",
            "cmtitle": "Category:Good_articles",
            "cmlimit": queryLimit,
            "format": "json",
        }
        logger.info(f"Fetch articles assessed as GA")
        baseUrl = f"https://{self.domain}/w/api.php"
        titles = []
        continueParams = {}
        while True:
            params.update(continueParams)
            logger.debug(f"GET URL {baseUrl} with params {params}")
            res = httpx.get(baseUrl, params=params, follow_redirects=True)
            res.raise_for_status()
            jsonResponse = res.json()

            for entry in jsonResponse["query"]["categorymembers"]:
                titles.append(entry["title"])

            logger.info(f"Fetched {len(titles)} GA titles")

            if ("continue" not in jsonResponse) or (len(titles) >= atLeastArticles):
                break

            continueParams = jsonResponse["continue"]
        return set(titles)

    @tenacity.retry(stop=tenacity.stop_after_attempt(retries))
    def getHTML(self, title: str) -> str:
        """Get parsoid output for a given title"""
        logger.info(f"Get HTML output for title: {title}")
        url = f"https://{self.domain}/api/rest_v1/page/html/{quote(title)}"
        res = httpx.get(url, follow_redirects=True)
        res.raise_for_status()
        return res.text

    def getTitles(self) -> set[str]:
        """Prepare set of articles"""
        goodArticles = self.getGoodArticles()
        return goodArticles

    def build(self) -> pd.DataFrame:
        """Build dataset"""
        logger.info(f"Building dataset")
        dataset = []
        progress = 0
        titles = self.getTitles()
        total = len(titles)
        for title in titles:
            progress += 1
            try:
                logger.debug(f"Adding title {title} - Progress {progress}/{total}")
                html = self.getHTML(title)
                summary = self.getSummary(title)
                entry = {
                    "title": title,
                    "html": html,
                    "extractedHtml": extractTextFromHtml(html),
                    "summary": summary,
                    "uuid": uuid.uuid4(),
                    "domain": self.domain,
                }
                dataset.append(entry)
            except:
                logger.error("Couldn't process title {title}")

        return pd.DataFrame.from_dict(dataset)
