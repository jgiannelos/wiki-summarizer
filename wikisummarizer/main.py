import click

from wikisummarizer.etl.dataset import DatasetBuilder


@click.command
@click.option("--domain", required=True)
@click.option("--output", required=True, type=click.Path())
def main(domain, output):
    builder = DatasetBuilder(domain=domain)
    df = builder.build()
    df.to_csv(output)


if __name__ == "__main__":
    main()
